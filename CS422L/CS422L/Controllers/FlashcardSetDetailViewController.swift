//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Jason Gallo on 2/8/22.
//

import UIKit

class FlashcardSetDetailViewController: UIViewController , UITableViewDataSource{

    @IBOutlet weak var cardTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardTableView.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Flashcard.getHardCodedFlashcards().count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: <#T##String#>, for: <#T##IndexPath#>)
    }
    
    
}


