//
//  FlashCardSetCollectionCell.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashcardSetCollectionCell: UICollectionViewCell
{
    @IBOutlet var textLabel: UILabel!
    
    @IBOutlet weak var flashcardTitle: UILabel!
    
    func setup(with cardTitle: FlashcardSet){
        flashcardTitle.text = cardTitle.title
    }
}
